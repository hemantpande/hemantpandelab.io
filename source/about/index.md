---
title: about
date: 2019-12-16 23:23:32
---

<!-- <img align="left" width="300" height="300" src="/images/my_image.jpg"> -->

<!-- ![half_duplex_network](/images/my_image.jpg) -->

I'm a language agnostic software developer, I love learning and exploring new languages, technologies. My key areas of interest are Cloud, Distributed computing, Computer networks, DevOps and Database technologies. I love all backend stuff. I like programming in GO these days.

I find Algorithms and data structures challenging, and I love solving problems.

I'm a staunch believer in Extreme programming and it's principles.

Over a period of time, I've also realized that clean code practices, TDD, DevOps culture and continuous refactoring is extremely crucial for long term projects.

Besides developing good software, I'm a good cook and I love playing and watching Cricket.

Please feel free to reach out to me for feedback, suggestions or even for a cup of coffee. 