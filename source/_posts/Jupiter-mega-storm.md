---
title: Jupiter's mega storm
date: 2020-05-10 22:43:53
tags: [astronomy, physics]
---

## Background

Hello, a disclaimer first, this post is not about [Jupyter notebooks](https://jupyter.org/). We will actually discuss and explore something about the Planet Jupiter in this post. 

As a kid, I was always quite fond of planets, stars and their physics. That learning and interest is perhaps the motivation for this post.   

You may ask, what's so interesting about it. Well, our universe is actually a very interesting place. There are so many unanswered questions about our universe, like 'How did our universe start?', 'What was before our universe started?', 'What's inside a black hole?', 'Can we travel faster than light?' and so on.... I am of course not an expert in this area. But through posts like this, my intention is to share a story and get you more curious about it. After all, if we can ask the right question, perhaps we may find the right answers.

## Planet Jupiter

Planet Jupiter is the largest planet in our solar system. Jupiter derives it's name from a God in Greek and Roman mythology. NASA's mission to study this planet was launched on August 5, 2011. It is interesting to note that this spacecraft was named Juno, who happens to be the wife of God Jupiter.

"The god Jupiter drew a veil of clouds around himself to hide his mischief, and his wife, the goddess Juno, was able to peer through the clouds and reveal Jupiter's true nature." - **NASA**

For many centuries, Jupiter's "Great Red Spot" has been a very interesting part of its study. 

![Jupiter's great red spot](/images/NASA14135-Jupiter-GreatRedSpot-Shrinks-20140515.jpg)
*(Source - Wikipedia)*

Some people also refer it as Jupiter's eye. It is so big, that it is visible through weakest of the telescopes. This large spot is in fact a **STORM** and in terms of size, it is as large as planet Earth. Juno space mission confirmed this, when it observed the storm close up.

Ok, so the storm is big, in fact very big, but how old it is? This storm is being observed consistently from the year 1830. But there are evidences of it's sightings well before 1830. According to a study, the storm was first observed in 1664. This indicates that it might be at least 350 years old.

But how can a storm last so long? largest storms on Earth typically last few weeks. So how do we have such a big 350-year old storm on Jupiter.

The answer to this question lies in Jupiter's atmosphere. But before going into Jupiter's atmosphere, we need to understand how storms get formed, in first place.

## Tropical storms

- Tropical storms always originate around the equator. 
- The ocean water near equator is warmer than the air flowing above it. 
- This vaporizes the water and the vapour rises above.
- The earth's rotation and spin around it's own axis acts against the momentum of rising air and gives the spin effect to the storm.
- As the vapour moves towards a low pressure area, which is typically a sea coast, it maintains it's spin effect and picks more warm vapour on it's way, after which it wrecks havoc on sea coasts. This warm water acts as fuel to the storm.

## Back to Jupiter's storm

Earth rotates once in 24 hours. Jupiter once in about 9.5 hours. 

The surface of Earth at the equator is rotating at about 1000 miles per hour, while Jupiter's equatorial cloud-tops are moving nearly 28,000 miles per hour.

That's enormous speed. Remember the earth spin gives the swirling effect to the storm. Image what a planet like Jupiter, which spinning so fast, would do to all it's gases in it's atmosphere. (Yes, Jupiter is a gas giant.)

It's the rapid spin of Jupiter, which also gives enormous gravity to the planet. Jupiter's surface gravity is 24.79 m/sec^2, compared to 9.8 m/sec^2 on Earth.

Perhaps the reason why it's considered as a symbol of strength and supreme power in many mythologies.

Until next time...!!