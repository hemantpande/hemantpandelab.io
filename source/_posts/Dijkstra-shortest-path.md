---
title: Dijkstra's shortest path algorithm
date: 2020-04-08 22:43:53
tags: [algorithms, graphs]
---

# Context
[Edsger W. Dijkstra](https://en.wikipedia.org/wiki/Edsger_W._Dijkstra) was one of the pioneer of early computer science. His research work related to distributed computing, parallel programming and many others, led to the foundations of new fields within computer science.

Among his most significant contributions was "Shortest path algorithm" for directed acyclic graphs with positive weights. I recently created a visualization of this algorithm using P5.js. 

It can be explored [here](https://hemantpande.github.io/dijkstras/)

Until next time!