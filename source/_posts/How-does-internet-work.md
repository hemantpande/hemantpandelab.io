---
title: How does internet work?
date: 2019-12-16 22:43:53
tags: [distributed, networking]
---

## Context

Internet is perhaps the biggest distributed system on the planet. It's vast and complex. What was actually a lab project, has now turned into one of the most significant creations of mankind that has changed our lives and the way businesses work.

In this blog post, I will make an attempt at explaining how the internet works, by trying to design a simple network from scratch. We'll not get into the heavy jargon or nitty-gritty of internet. People have ended up writing books on that. Instead we'll focus on simple principles.

When internet was born there were only 16 computers in that network, that's it. That is also probably the reason why the inventors thought that the IPV4 address space will never run out of addresses. But internet was a hit and has grown to a scale that was not imagined earlier. 

First up, let's try to design a network. We'll try multiple approaches, weigh their pros and cons and try to improvise was we make progress.

## Basic addresses

Let's assign a 4-bit address to each of these 16 computers. The address space in this case would look something like this:-

| Computer        | Address       |
| -------------   |:-------------:|
| S0              | 0000          |
| S1              | 0001          |
| S2              | 0010          |
| S3              | 0011          |
| S4              | 0100          |
| S5              | 0101          |
| S6              | 0110          |
| S7              | 0111          |
| S8              | 1000          |
| S9              | 1001          |
| S10             | 1010          |
| S11             | 1011          |
| S12             | 1100          |
| S13             | 1101          |
| S14             | 1110          |
| S15             | 1111          |


## Approach 1

Now that every machine knows what it's address is, let's try to design a basic network.

The most simplest way is to connect each machine with every other machine in our network. The network diagram in that case would look something like this:

![half_duplex_network](/images/half_duplex.png)

I've just connected S0 with rest of the machines in the network and this already looks messy. The full implementation of this approach (or the diagram) would be when we connect each server to every other server. Such a n/w communication can be called as half-duplex.

There are 2^32 (4,294,967,296) IPV4 addresses in the world. If we design the internet using this approach, we would need copper wires in the order 10^20. With all the machines spread widely across the world, I don't think we have enough copper on earth to suffice that kind of need. So, we can conclude that approach doesn't work. (One take away is we still have a way to establish a secure p2p connection in a network. ALthough it works on TCP/IP. But we'll get to that, we're not there yet.)

## Approach 2

So we learned that can't every machine to every other machine, but we could delegate that job to a dedicated machine which by some magic knows about all the machines in the network (we'll get to what that magic is in a moment). Let's try that.

![star_topology](/images/star_network.png)

The central machine in this case would need 16 ports, each for a machine in the network. If machine 0000 wants to send a packet to machine 0111, then the central machine (or should we call it a **router**) will look at the outgoing IP address in the packet and route it to corresponding port. How it does this, yes, that magic is called, **routing table** (which could look something like this)

| Computer        | Address       | Port number   |
| -------------   |:-------------:|:-------------:|
| S0              | 0000          | 1             |
| S1              | 0001          | 2             |
| S2              | 0010          | 3             |
| S3              | 0011          | 4             |
| S4              | 0100          | 5             |
| S5              | 0101          | 6             |
| S6              | 0110          | 7             |
| S7              | 0111          | 8             |
| S8              | 1000          | 9             |
| S9              | 1001          | 10            |
| S10             | 1010          | 11            |
| S11             | 1011          | 12            |
| S12             | 1100          | 13            |
| S13             | 1101          | 14            |
| S14             | 1110          | 15            |
| S15             | 1111          | 16            |

This could potentially work. But there's a problem. Problem is that central router, right there in the middle. 
What if it fails?
Where would you place it on earth, considering we have 2^32 machines to handle?
And the most important consideration, Can we build a machine which has 2^32 ports?

I don't think so.. There are hardware and performance limitations.

## Approach 3

Fine, no problem, we can improvise, can't we. We'll have multiple routers in the middle, let's do that and see what happens.

![scaled_star_topology](/images/extended_star.png)

We've introduced 4 routers in the middle now and each of the router is connected to 4 machines. We have increased the reliability of the n/w now, if one router goes down, we only have a subset of n/w going down, not the whole. Also we can now scale, stick in more routers and attach more machines to it. But there is a fundamental problem and we'll look at it through an example.

Let's say machine 0000 wants to send a packet to machine 0111. 0000 sends the packet to the router it is connected to. Router R1 looks at the route table it has, and finds that the machine 0111 is connected to router R3. The route table in this case would look like this

| Computer        | Address       | Router   | Port number   |
| -------------   |:-------------:|:--------:|:-------------:|
| S0              | 0000          | R1       | 1             |
| S1              | 0001          | R1       | 2             |
| S2              | 0010          | R1       | 3             |
| S3              | 0011          | R1       | 4             |
| S4              | 0100          | R2       | 1             |
| S5              | 0101          | R2       | 2             |
| S6              | 0110          | R2       | 3             |
| S7              | 0111          | R2       | 4             |
| S8              | 1000          | R3       | 1             |
| S9              | 1001          | R3       | 2             |
| S10             | 1010          | R3       | 3             |
| S11             | 1011          | R3       | 4             |
| S12             | 1100          | R4       | 1             |
| S13             | 1101          | R4       | 2             |
| S14             | 1110          | R4       | 3             |
| S15             | 1111          | R4       | 4             |

For this to work, we need to ensure that each router has the exact same copy of the route table. Also if a new machine gets added on, let's say R4, then the all the routers need to have the updated copy of the route table. This could be a problem considering the large geography of earth. We can face latency issues and inconsistent data (route tables) can lead to failure of the system. In terms of distributed system, we can say, that our system is scalable, reliable, but not eventually consistent.

Also this could lead to very long route tables when the n/w grows. (2^32, to be practical) 

## Approach 4

Is there a way to get around this? Let's see, we haven't given up till now, (and we never will).

At this point we need to make a very important decision and that is related to geography. Let's say that any address starting with 

00- is the responsibility of R1
01- is the responsibility of R2
10- is the responsibility of R3
11- is the responsibility of R4

(This is true in real internet systems as well. IP addresses in one geographical location start with a common address space. That is how they narrow down your location based on your IP)

By making this decision, let's see what happens to our route table.

| Computer        | Address       | Router   | Port number   |
| -------------   |:-------------:|:--------:|:-------------:|
| S0              | 0000          | R1       | 1             |
| S1              | 0001          | R1       | 2             |
| S2              | 0010          | R1       | 3             |
| S3              | 0011          | R1       | 4             |
| *               | 01*           | R2       | *             |
| *               | 10*           | R3       | *             |
| *               | 11*           | R3       | *             |
_(* denotes anything beyond this)_

The above route table is of router R1. And it has got a lot shorter. 

What the route table is basically saying, is that 

"**I have information of machines connected to me. But for other addresses, I don't know where they are, but I know which router can help route it**"

This is an important statement and an important milestone in our design process. When we construct the route tables of rest of the routers, they will have a similar structure, as well. When we add a new machine to a router, it simply has to update it's own route table. Other routers don't care about this new addition, as long as it is in the address space allocated to that particular router. This is a very powerful concept in networking and it's called as "**Sub-netting**. 

## Scaling beyond limits

Now that we have a structure in place, our design is scalable. We'll see a small example of how we can do that. 

If we increase one more bit in our address space, we'll have total 2^4 = 32 addresses. We can scale such a network by joining 2 sub-networks of 4-bit address space each.

Anything starting with the highest bit as 0 would be sub-network 1. 

And anything starting with highest bit as 1 would be sub-network 2.

The internal structure of each of these sub-networks would be similar to a 4-bit network.

## Conclusion

And this is how at a very high level, internet is structured. A perfect design is a balance between different elements of a distributed system, consistency, reliability, latency, router hardware scalability and performance. The study of internet is quite a vast topic, and I'll blog about more specific topics in days to come.

Until next time! 
